class Features22 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initFeatures22()
    }

    initFeatures22 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Features 22")
    }

}

window.customElements.define('fir-features-22', Features22, { extends: 'div' })
