module.exports = {
    'name'  : 'Features 22',
    'camel' : 'Features22',
    'slug'  : 'features-22',
    'dob'   : 'Features_22_1440',
    'desc'  : 'Section with two images and title, text, cta.',
}