<!-- Start Features 22 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Section with two images and title, text, cta. -->
@endif
<div class="features-22 {{ $classes }}"  is="fir-features-22">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="features-22__wrap">
    <div class="features-22__copy">
      <h2 class="features-22__title">
        {{ $title ?? 'Lorem ipsum dolor sit amet.' }}
      </h2>
      <p class="features-22__text">
          {{ $text ?? 'Quidam officiis similique sea ei, vel tollit indoctum efficiendi ei, at nihil tantas platonem eos.' }}
      </p>
      <a href="{{ $cta['url'] }}" class="btn btn--hollow">{{ $cta['title'] }}</a>
    </div>

    <div class="features-22__panels">
      <div class="features-22__panel">
        <img src="{{ $image_1['url'] }}" class="features-22__panel-image" alt="{{ $image_1['alt'] }}">
        <p class="features-22__panel-caption">{{ $image_caption_1 ?? 'Lorem ipsum dolor sit amet.' }}</p>
      </div>

      <div class="features-22__panel">
        <img src="{{ $image_2['url'] }}" class="features-22__panel-image" alt="{{ $image_2['alt'] }}">
        <p class="features-22__panel-caption">{{ $image_caption_2 ?? 'Lorem ipsum dolor sit amet.' }}</p>
      </div>
    </div>
  </div>
</div>
<!-- End Features 22 -->
