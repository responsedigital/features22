<?php

namespace Fir\Pinecones\Features22;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Features22',
            'label' => 'Pinecone: Features 22',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "Section with two images and title, text, cta."
                ],
                [
                    'label' => 'Content',
                    'name' => 'contentTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Title',
                    'name' => 'title',
                    'type' => 'text',
                    'required' => 1
                ],
                [
                    'label' => 'Text',
                    'name' => 'text',
                    'type' => 'textarea',
                    'required' => 1
                ],
                [
                    'label' => 'CTA',
                    'name' => 'cta',
                    'type' => 'link'
                ],
                [
                    'label' => 'Image Blocks',
                    'name' => 'imagesTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Image Block 1',
                    'name' => 'image_1',
                    'type' => 'image',
                    'required' => 1
                ],
                [
                    'label' => 'Image Caption 1',
                    'name' => 'image_caption_1',
                    'type' => 'text'
                ],
                [
                    'label' => 'Image Block 2',
                    'name' => 'image_2',
                    'type' => 'image',
                    'required' => 1
                ],
                [
                    'label' => 'Image Caption 2',
                    'name' => 'image_caption_2',
                    'type' => 'text'
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'flipHorizontal',
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
